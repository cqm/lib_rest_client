# CQM REST Client for PHP

### REST client to make requests to the CQM APIs programmatically.

The RestClient::class provides encapsulation for HTTP verbs:

**get(*string* $endpoint, *array* $params = array())**

**post(*string* $endpoint, *array* $params = array(), *array* $files = array())**

**put(*string* $endpoint, *array* $params = array())**

...

It also handles API authentication. Include your API keys when creating the RestClient instance.

Basic example (send one e-mail with CC and files attached)

```php
<?php 

require(__DIR__.'/../vendor/autoload.php');

use CQM\Libraries\RestClient\RestClient;

// Set your credentials
$apiKey = 'your api key';
$apiSecret = 'your api secret';
$baseUri = 'http://example.com';
// Get the client
$restClient = new RestClient($apiKey, $apiSecret, $baseUri);
// Set endpoint and data
$endpoint = '/api/v1/emails';
$data = array(
    'application_id' => 1,
    'email' => 'johndoe@example.com',
    'first_name' => 'John',
    'last_name' => 'Doe',
    'subject' => 'Sent from CQM REST Client',
    'body' => 'E-mail body',
    'server_id' => 3,
    'cc' => '[{"email":"foo@example.com", "first_name": "Foo", "last_name": "Bar"}]'
);

try {
    // File attachments
    $files = array(
        $restClient->makeCurlFile('/path/to/file', 'application/pdf', 'yourfilename.pdf'),
    );
    // POST request example
    $response = $restClient->post($endpoint, $data, $files);
    
    print($response->getPlainResponse());

} catch (\Exception $ex) {
    print($ex->getMessage());
}
```

