<?php

namespace CQM\Libraries\RestClient;

use CQM\Libraries\RestClient\Util\UtilApi;
use CQM\Libraries\RestClient\Util\UtilMessage;
use CQM\Libraries\RestClient\Http\Request;
use CQM\Libraries\RestClient\Http\Response;
use CQM\Libraries\RestClient\Exception\CurlException;
use CQM\Libraries\RestClient\Exception\BadResponseException;
use CQM\Libraries\RestClient\Exception\ErrorResponseException;
use CQM\Libraries\RestClient\Exception\AuthenticationException;
use CQM\Libraries\RestClient\Exception\RestClientException;

/**
 * CQM REST Client
 * @author Akira Serikawa <aserikawa@chequemotiva.com>
 */
class RestClient
{
    const CONTENT_TYPE_APPLICATION_JSON = 'application/json';
    const CONTENT_TYPE_X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const CONTENT_TYPE_MULTIPART_FORM_DATA = 'multipart/form-data';
    const HTTPS = 'https';
    const HTTP = 'http';
    const BASE_URI = '';
    const USER_AGENT = 'CQM REST Client';
    const CURL_VERYFYHOST = 2;
    const PHP_REQUIRED_VERSION = '7.0.7';
    const CURL_REQUIRED_VERSION = '7.45.0';
    
    private $apiKey;
    
    private $apiSecret;
    
    private $protocol;
    
    private $baseUri;
    
    private $authToken;
    
    private $authVersion = 'hmac_sha256';

    private $contentType;

    private $headers = array();

    private $boundary;
    
    /**
     * @var array
     */
    private $config = array(
        'endpoint' => '',
        'timeout' => 30,
        'verify_ssl' => false,
        'ssl_certs_bundle' => __DIR__.'/../ssl/cacert.pem',
    );

    /**
     * Create the client
     * @param string $apiKey    Application API key
     * @param string $apiSecret Application API secret
     * @param string $protocol  Default protocol to send requests. Possible values: 'http', 'https'.
     * @param array  $config    Other custom configuration
     * @param string $contentType Content-Type
     */
    public function __construct($apiKey, $apiSecret, $baseUri = self::BASE_URI, $protocol = self::HTTPS, $config = array(), $contentType = self::CONTENT_TYPE_X_WWW_FORM_URLENCODED, $boundary = '')
    {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->protocol = $protocol;
        if (!empty($config)) {
            $this->setConfig($config);
        }
        $this->baseUri = $baseUri; 
        $this->contentType = $contentType;
        $this->boundary = $boundary;
    }

    /**
     * Replace the default config values with custom ones
     * @param array $config Custom configuration
     */
    private function setConfig(array $config)
    {
        $config = array_replace($this->config, $config);
        $config['endpoint'] = rtrim($config['endpoint'], '/');

        $this->config = $config;

        return $this;
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;

        return $this;
    }

    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;

        return $this;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    public function pullHeaders()
    {
        $headers       = $this->headers;
        $this->headers = [];

        return $headers;
    }

    public function addHeaders($headers)
    {
        $this->headers[] = $headers;

        return $this;
    }

    public function setContentType($contentType)
    {
        if ($contentType === self::CONTENT_TYPE_MULTIPART_FORM_DATA && !empty($this->boundary)) {
            $contentType = "{$contentType}; boundary={$this->boundary}";
        }
        
        $this->contentType = $contentType;

        return $this;
    }

    public function getContentType()
    {
        return $this->contentType;
    }

    public function setAuthVersion($authVersion)
    {
        $this->authVersion = $authVersion;

        return $this;
    }

    public function getAuthVersion()
    {
        return $this->authVersion;
    }

    /**
     * Generate the authentication token (X-AUTH-TOKEN)
     * This token must be included in the headers of every request
     */
    private function generateAuthenticationToken()
    {
        if (is_null($this->apiKey) || is_null($this->apiSecret)) {
            throw new AuthenticationException(UtilMessage::INVALID_AUTH_CREDENTIALS, 403);
        }

        $this->authToken = UtilApi::generateToken($this->apiKey, $this->apiSecret);

        return $this;
    }

    /**
     * Get the current authentication token
     */
    public function getAuthenticationToken()
    {
        return $this->authToken;
    }

    /**
     * Performs a GET request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function get($resource, array $params = [])
    {
        return $this->doRequest('GET', $resource, $params);
    }

    /**
     * Performs a POST request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function post($resource, array $params = [], array $files = [])
    {
        return $this->doRequest('POST', $resource, $params, $files);
    }

    /**
     * Performs a PUT request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function put($resource, array $params = [])
    {
        return $this->doRequest('PUT', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function patch($resource, array $params = [])
    {
        return $this->doRequest('PATCH', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function link($resource, array $params = [])
    {
        return $this->doRequest('LINK', $resource, $params);
    }

    /**
     * Performs a PATCH request
     *
     * @param string $resource Resource path
     * @param array $params Request parameters
     * @return Response Response
     */
    public function unlink($resource, array $params = [])
    {
        return $this->doRequest('UNLINK', $resource, $params);
    }

    /**
     * Performs a request
     *
     * @param string $method HTTP method (GET, POST, PUT, PATCH, LINK, UNLINK)
     * @param string $resource Resource path
     * @param array $params Request params
     * @return Response Response
     */
    protected function doRequest($method, $resource, array $params, array $files = [])
    {
        list($resource, $params) = $this->processResourceAndParams($resource, $params);

        $try = 0;
        do {
            $curl = $this->curlInit($resource, $params, $method, $files);
            $result = $this->curlExec($curl);
            $response = $this->createResponse($result);
        } while(++$try < 3 && $response->isError() && $response['code'] === 4005);
        // ^ Retry the request up to 3 times in case of duplicated request error

        if ($response->isError()) {
            throw new ErrorResponseException($response);
        }

        return $response;
    }

    /**
     * Replaces variables in the resource string
     *
     * @param string $resource
     * @param array $params
     * @return array
     */
    protected function processResourceAndParams($resource, array $params)
    {
        $resource = trim($resource, '/');

        foreach ($params as $key => $value) {
            if (!is_scalar($value)) {
                continue;
            }
            // TODO: ??
            $resource = str_replace('{'. $key .'}', urlencode((string)$value), $resource, $replaced);

            if ($replaced) {
                unset($params[$key]);
            }
        }

        return array($resource, $params);
    }

    protected function curlInit($resource, array $params, $method, array $files)
    {
        $curl = $this->curlCreate();
        $curl = $this->curlSetHeaders($curl, $files);
        $curl = $this->curlSetRequest($curl, $resource, $params, $method, $files);

        return $curl;
    }

    protected function curlCreate()
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        
        /**
         * @deprecated 1.0.7
         * @deprecated No longer used by internal code and not recommended.
         */
        // if ($this->checkMinimumPhpAndCurlVersions()) {
            // curl_setopt($curl, CURLOPT_DEFAULT_PROTOCOL, $this->protocol);
        // }

        if ($this->config['verify_ssl']) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->config['verify_ssl']);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->config['verify_ssl'] ? self::CURL_VERYFYHOST : 0);
            curl_setopt($curl, CURLOPT_CAINFO, $this->config['ssl_certs_bundle']);
        }

        return $curl;
    }

    protected function curlSetHeaders($curl, array $files)
    {
        // Build array headers
        $this->addContentTypeHeaders($files);
        $this->addAuthenticationHeaders();
        
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->pullHeaders());

        return $curl;
    }

    private function addContentTypeHeaders($files)
    {
        if (!empty($files)) {
            $this->setContentType(self::CONTENT_TYPE_MULTIPART_FORM_DATA);
        }
        
        $this->addHeaders("Content-Type: {$this->getContentType()}");
    }

    /**
     * Set the request headers
     */
    private function addAuthenticationHeaders()
    {
        $this->generateAuthenticationToken();
        
        $authVersion = $this->getAuthVersion();
        $authToken = $this->getAuthenticationToken();

        $this->addHeaders("X-Auth-Version: ${authVersion}");
        $this->addHeaders("X-Auth-Token: ${authToken}");
    }

    protected function curlSetRequest($curl, $resource, array $params, $method, $files)
    {
        // Set url + endpoint
        $url = $this->baseUri . '/' . $resource;

        /* Params */
        switch (strtoupper($method)) {
            case Request::HTTP_GET:
                $url = $url . (empty($params) ? '' : '?' . http_build_query($params));
                break;
                
            case Request::HTTP_POST:
            
                $params = $this->buildPostParamsBasedOnContentType($params, $files);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
                break;

            default:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, strtoupper($method));
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
                break;
        }

        curl_setopt($curl, CURLOPT_URL, $url);

        return $curl;
    }

    protected function curlExec($curl)
    {
        $response = [
            'body' => curl_exec($curl),
            'curl_errno' => curl_errno($curl),
            'curl_error' => curl_error($curl),
            'http_code' => curl_getinfo($curl, CURLINFO_HTTP_CODE),
        ];

        curl_close($curl);

        return $response;
    }

    protected function createResponse($result)
    {
        if ($result['curl_errno'] !== 0) {
            throw new CurlException($result['curl_error'], $result['curl_errno']);
        }

        $body = json_decode($result['body'], true);
        
        if (json_last_error()) {
            throw new BadResponseException($result['body'], json_last_error_msg(), json_last_error());
        }

        return new Response($result['body'], $body, $result['http_code']);
    }

    private function buildPostParamsBasedOnContentType($postParams, $files)
    {
        if ($this->getContentType() === self::CONTENT_TYPE_X_WWW_FORM_URLENCODED) {
            $postParams = \http_build_query($postParams);
        } else if ($this->getContentType() === self::CONTENT_TYPE_MULTIPART_FORM_DATA) {
            $postParams = \array_merge($postParams, $files);
        }

        return $postParams;
    }

    public function makeCurlFile($filepath, $mimeType, $filename)
    {
        if (empty($filepath) || !\realpath($filepath)) {
            throw new RestClientException(sprintf("[ERROR] File could not be found in path: %s", $filepath), 400);
        }

        return \curl_file_create($filepath, $mimeType, $filename);
    }

    /**
     * @deprecated 1.0.7
     * @deprecated No longer used by internal code and not recommended.
     */
    private function checkMinimumPhpAndCurlVersions()
    {
        if (version_compare(phpversion(), self::PHP_REQUIRED_VERSION, '<')) {
            return false;
        }
        
        if (function_exists('curl_version') && version_compare(curl_version()['version'], self::CURL_REQUIRED_VERSION, '<')) {
            return false;
        }
        return true;
    }
}
