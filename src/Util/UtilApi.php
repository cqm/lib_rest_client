<?php

namespace CQM\Libraries\RestClient\Util;

use CQM\Libraries\Security\Signature;

class UtilApi
{
    const TOKEN_SEPARATOR = '#';

    public static function generateToken($apiKey, $apiSecret)
    {
        $signature = Signature::makeHmacSha256($apiSecret, $nonce, $ts);

        return implode(self::TOKEN_SEPARATOR, array($apiKey, $nonce, $ts, $signature));
    }
}
