<?php

namespace CQM\Libraries\RestClient\Http;

class Response implements \ArrayAccess
{
    private $plain_response;

    private $data;

    private $http_status_code;

    public function __construct($plain_response, array $data, $http_status_code)
    {
        $this->plain_response = $plain_response;
        $this->data = $data;
        $this->http_status_code = $http_status_code;
    }

    /**
     * Indicates if the response is a success
     */
    public function isOk()
    {
        return $this->http_status_code >= 200 && $this->http_status_code < 300;
    }

    /**
     * Indicates if the response is an error
     */
    public function isError()
    {
        return !$this->isOk();
    }

    /**
     * Returns plain API response
     */
    public function getPlainResponse()
    {
        return $this->plain_response;
    }

    /**
     * Returns processed API response as an array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Returns HTTP status code
     */
    public function getHttpStatusCode()
    {
        return $this->http_status_code;
    }

    /**
     * Returns a value from the processed response.
     * You can use dot notation for nested arrays. Example:
     *
     * $response->get('parent.children.leaf');
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $parts = explode('.', $key);
        $return = $this->data;

        foreach ($parts as $part) {
            if (!is_array($return) || !array_key_exists($part, $return)) {
                return $default;
            } else {
                $return = $return[$part];
            }
        }

        return $return;
    }

    /**
     * Checks if a value exists in the response.
     * You can use dot notation for nested arrays. Example:
     *
     * $response->has('parent.children.leaf');

     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        $parts = explode('.', $key);
        $return = $this->data;
        foreach ($parts as $part) {
            if (!is_array($return) || !array_key_exists($part, $return)) {
                return false;
            }
        }

        return true;
    }

    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        throw new \LogicException();
    }

    public function offsetUnset($offset)
    {
        throw new \LogicException();
    }
}
