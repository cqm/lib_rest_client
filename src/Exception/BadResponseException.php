<?php

namespace CQM\Libraries\RestClient\Exception;

class BadResponseException extends RestClientException
{
    /** @var string */
    private $response_text;

    public function __construct($response_text, $message = '', $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->response_text = $response_text;
    }

    /**
     * Returns plain response text
     * @return string
     */
    public function getResponseText()
    {
        return $this->response_text;
    }
}
