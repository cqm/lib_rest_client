<?php

namespace CQM\Libraries\RestClient\Exception;

use CQM\Libraries\RestClient\Http\Response;

class ErrorResponseException extends RestClientException
{
    /**
     * @var Response
     */
    private $response;

    public function __construct(Response $response, \Throwable $previous = null)
    {
        parent::__construct($response->getPlainResponse(), $response->getHttpStatusCode(), $previous);

        $this->response = $response;
    }

    /**
     * Returns the response
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
