<?php

namespace CQM\Libraries\RestClient\Exception;

class CurlException extends RestClientException
{
    public function __construct($message, $code, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
